//    merkle-rs, a merkle tree library implemented in Rust
//    Copyright (C) 2016 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

mod benches {
    extern crate test;
    use self::test::Bencher;
    use MerkleTree;
    use sha3::Sha3;
    use sha2::Sha512;
    #[bench]
    fn bench_1025_nodes_creation(b: &mut Bencher) {
        b.iter(|| MerkleTree::new(1..1026));
    }
    #[bench]
    fn bench_1024_nodes_creation(b: &mut Bencher) {
        b.iter(|| MerkleTree::new(1..1025));
    }
    #[bench]
    fn bench_single_node_creation(b: &mut Bencher) {
        b.iter(|| MerkleTree::new(&[1]));
    }
    #[bench]
    fn bench_1025_nodes_hash_sha512(b: &mut Bencher) {
        let mut m_tree=MerkleTree::new(1..1026);
        b.iter(|| m_tree.get_root_hash(Sha512::new()));
    }
    #[bench]
    fn bench_1024_nodes_hash_sha512(b: &mut Bencher) {
        let mut m_tree=MerkleTree::new(1..1025);
        b.iter(|| m_tree.get_root_hash(Sha512::new()));
    }
    #[bench]
    fn bench_1025_nodes_hash_sha3_512(b: &mut Bencher) {
        let mut m_tree=MerkleTree::new(1..1026);
        b.iter(|| m_tree.get_root_hash(Sha3::sha3_512()));
    }
    #[bench]
    fn bench_1024_nodes_hash_sha3_512(b: &mut Bencher) {
        let mut m_tree=MerkleTree::new(1..1025);
        b.iter(|| m_tree.get_root_hash(Sha3::sha3_512()));
    }
    #[bench]
    fn bench_2048_nodes_hash_sha3_512(b: &mut Bencher) {
        let mut m_tree=MerkleTree::new(1..2049);
        b.iter(|| m_tree.get_root_hash(Sha3::sha3_512()));
    }
    #[bench]
    fn bench_2049_nodes_hash_sha3_512(b: &mut Bencher) {
        let mut m_tree=MerkleTree::new(1..2050);
        b.iter(|| m_tree.get_root_hash(Sha3::sha3_512()));
    }
    //#[bench]
    //fn bench_1000000_nodes_hash_sha3_512(b: &mut Bencher) {
    //    let mut m_tree=MerkleTree::new(0..1000000);
    //    b.iter(|| m_tree.get_root_hash(Sha3::sha3_512()));
    //}
}
