//    merkle-rs, a merkle tree library implemented in Rust
//    Copyright (C) 2016 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//! The merkle crate provides a Merkle tree object.
//! 
//! SHA2 and SHA3 functions are reexported from crypto-rs for convenience.
//!
//! # Examples
//! Basic usage example:
//!
//! ```rust
//! use merkle::MerkleTree;
//! use merkle::sha3::Sha3;
//! let words = vec!["example","words"];
//! let mut m_tree = MerkleTree::new(words);
//! assert_eq!(
//!     m_tree.get_root_hash(Sha3::sha3_512()),
//!     "00766ae74c3ba18189e8ed33b218b1f491b99d\
//!     cccd13ce046d97e30bdcb2d4bbe0dcaeb29dbe5\
//!     88fa5f7b71f36729be21f5077c89c62380f1a32\
//!     2db23c1c3edc"
//! );
//! ```
//! 
//! The created Merkle tree has two leaves with contents "example" and "words":
//!
//! ```notrust
//! sha3("examplewords")
//!        /    \
//!       /      \
//!   example   words
//! ```
//!
//! The sha3-512 hash of the root node is the hash of both of it's children's
//! contents concatenated together.
//! In this case it would be sha3-512("examplewords").
//! 
//! Three-leaf tree:
//!
//! ```rust
//! use merkle::MerkleTree;
//! use merkle::sha3::Sha3;
//! let words = vec!["one","two","three"];
//! let mut m_tree = MerkleTree::new(words);
//! assert_eq!(
//!     m_tree.get_root_hash(Sha3::sha3_512()),
//!     "428e0165d9540d82a928ca1da1e9b3358988c3\
//!     52ead4cfe34f9955656b316004d06c055962457\
//!     be7ee6b4c5291a4361fd64594522d9bc213957f\
//!     d5f7cb0d1c50"
//! );
//! ```
//!
//! If a tree doesn't have an even number of leaves the contents of the extra leaf will
//! be concatenated to itself when it is hashed.
//! The tree in the three-leaf example ends up looking like this:
//!
//! ```notrust
//!  sha3(sha3("onetwo")sha3("threethree"))
//!               /                \
//!              /                  \
//!             /                    \
//!            /                      \
//!           /                        \
//!  sha3("onetwo")           sha3("threethree")
//!        /    \                       |
//!       /      \                      |
//!     one      two                  three
//! ```
#![warn(missing_docs)]
//#![feature(test)] //Uncomment to run benchmarks
extern crate crypto;
use crypto::digest;
pub use crypto::sha3;
pub use crypto::sha2;
use std::iter::FromIterator;
mod tests;
#[cfg(feature="bench")]
mod bench;
#[derive(Clone,Debug,PartialEq)]
///A Merkle tree implementation
pub struct MerkleTree {
    root: Box<Node>,
    words: Vec<String>
}
impl MerkleTree {
    ///Returns the root hash of the given Merkle tree.
    ///
    ///The given digest function is used to build the hash tree.
    pub fn get_root_hash<T: digest::Digest + Copy>(&mut self, hasher: T) -> String {
        self.root.calculate_hash(hasher)
    }
    ///Returns true if given hash matches the root hash of the given tree.
    ///
    ///Calling this function has the same result as 
    ///`tree.get_root_hash(hasher)==expected_hash`.
    pub fn validate<H: digest::Digest + Copy,S: ToString>(&mut self, hasher: H, expected_hash: S) -> bool {
        self.get_root_hash(hasher)==expected_hash.to_string()
    }
    ///Convenience function wrapper around `from_iter()`
    pub fn new<T: ToString,I: IntoIterator<Item=T>>(words: I) -> MerkleTree {
        MerkleTree::from_iter(words)
    }
    ///This function adds a node to the tree.
    ///
    ///Currently the entire tree is rebuilt, but in a future version
    ///the tree will only be rebuilt as necessary.
    pub fn push<T: ToString>(&mut self, word: T) {
        //Nodes are added to the right side of the tree
        //TODO: Make use of previously used hashes
        self.words.push(word.to_string());
        let words = self.words.clone();
        *self.root = *(MerkleTree::new(words).root);
    }
    ///Removes and returns the last leaf in the tree
    ///
    ///Unlike push, this does not create a new tree and preserves hashes
    pub fn pop(&mut self) -> Option<String> {
        //let ref mut node = self.root;
        //loop {
        //    match (&mut node.rhs) {
        //        (&mut None,&mut None) => { //This node is a leaf
        //            break; //Prev_node points to the parent of the node to be removed
        //        },
        //        (&mut Some(ref mut c),&mut None) => {
        //            c.clear();
        //            prev_node = node;
        //            node = c;
        //        }
        //        _ => {}
        //    }
        //}
        self.root.remove_last_node();
        if self.root.rhs.is_none(){
            if self.root.lhs.is_some() && !self.root.lhs.as_mut().unwrap().is_leaf() { 
                //tree is not malformed and has a depth of more than 1
                let left_tree = self.root.lhs.clone().unwrap(); 
                //left_tree contains a reference to root, so I need to use Arc or have
                //multiple copies of the data
                self.root = left_tree;
            }
            
        }
        self.words.pop()
    }
}
impl<T: ToString> FromIterator<T> for MerkleTree {
    fn from_iter<I: IntoIterator<Item=T>>(iter: I) -> MerkleTree { //Creates a tree from given words
        let mut top_level_nodes = iter.into_iter().map(
            |w| Node::new(Some(w.to_string()))
            ).collect::<Vec<Node>>().into_iter();
        let argh = top_level_nodes.clone();
        //top_level_nodes now contains the leaf nodes
        let mut next_level_nodes = Vec::new();
        let root_node: Node;
        loop { 
            match (top_level_nodes.next(),top_level_nodes.next()) {
                (None,None) => { //Work done
                    if next_level_nodes.len() <= 1 {
                        root_node = next_level_nodes.pop().unwrap_or(Node::new(None));
                        break; //Finished building tree
                    } else {//Otherwise next level is built
                        top_level_nodes = next_level_nodes.into_iter();
                        next_level_nodes = Vec::new();
                    }
                    }, 
                (Some(c),None) | (None, Some(c))=>  next_level_nodes.push(
                    Node{
                        lhs: Some(Box::new(c)),
                        rhs: None,
                        contents: None
                    }),
                (Some(l),Some(r)) => 
                    next_level_nodes.push(
                        Node{
                            lhs: Some(Box::new(l)),
                            rhs: Some(Box::new(r)),
                            contents: None
                    })
            }
        }
        return MerkleTree { root: Box::new(root_node),  
            words: argh.filter(|w| w.contents.is_some()).map(
                |w| w.contents.unwrap().to_string()
                ).collect()
        };
    }
}
#[derive(Clone,Debug,PartialEq)]
struct Node {
    lhs: Option<Box<Node>>, //If both are none then node is a leaf
    rhs: Option<Box<Node>>, //If one is none then duplicate its contents
    contents: Option<String> //Contains string to hash or hash
}
impl Node {
    fn remove_last_node(&mut self) -> bool {
        //Returns true if there is a chain of single children including the final node
        self.clear(); //Clear node's hash
        match self.child_is_leaf() {
            None => {return false}, //This means remove_last_node was called on a 
                                    //single node tree
                                    //It should be safe to silently fail here
            Some(false) => {
                //This node has non-leaf children, it's safe to recurse into them
                let mut delete_left = false; //Delete after borrows are done
                let mut delete_right =false; 
                match (&mut self.lhs,&mut self.rhs) {
                    (&mut Some(ref mut c),  &mut None) => {
                        //Recurse into left side if right side does not exist
                        //If there are repeated lefts then nodes must be deleted
                        //so there are no arms of the tree without content leaves
                        delete_left = c.remove_last_node(); //delete left side
                    },
                    (&mut Some(_), &mut Some(ref mut r)) => {
                        delete_right = r.remove_last_node();
                        //Because tree is left aligned we can safely assume
                        //the branch to be removed is on the right side
                    },
                    (&mut None, &mut Some(_)) => {
                        panic!("Tree not left aligned");
                    },
                    (&mut None,&mut None) => {
                        unreachable!();
                    }
                }
                if delete_left {
                    self.lhs =None;
                    return true;
                } else if delete_right {
                    self.rhs = None;
                }
                return false;
            },
            Some(true) => {
                //One of this node's children is a leaf node
                match (self.lhs.is_some(),self.rhs.is_some()) {
                    (true,true) => {
                        //delete right side
                        self.rhs = None;
                        return false;
                    },
                    (true,false) => {
                        //lhs is okay to use
                        self.lhs = None;
                    },
                    (false,true) => {
                        panic!("Tree not left aligned");
                    },
                    (false,false) => {
                        unreachable!();
                    }
                }
                return true;
            }
        }
    }
    fn new(contents: Option<String>) -> Node {
        Node { lhs: None, rhs: None, contents: contents}
    }
    fn number_of_children(&self) -> isize {
        match (&self.lhs,&self.rhs) {
            (&None,&None) => 0,
            (&Some(_),&Some(_)) => 2,
            _ => 1
        }
    }
    //Returns true if node has no chilren
    fn is_leaf(&self) -> bool {
        self.number_of_children() == 0
    }
    //Returns true if either child of the node is a leaf
    fn child_is_leaf(&self) -> Option<bool>{
        match (&self.lhs,&self.rhs) {
            (&None,&None) => None,
            (&Some(ref l),&Some(ref r)) => 
                Some(l.is_leaf() || r.is_leaf()),
            (&Some(ref c),&None) | (&None, &Some(ref c)) =>
                Some(c.is_leaf())
        }
    }
    fn clear(&mut self) {
        self.contents = None;
    }
    fn calculate_hash<T: digest::Digest + Copy>(&mut self, mut hasher: T) -> String {
        let final_string: String;
        match self.contents {
            Some(ref c) => { return c.clone(); }, //return contents for nodes to hash
            None => { //node needs childrens contents
                match (&mut self.lhs,&mut self.rhs) {
                    (&mut Some(ref mut l), &mut Some(ref mut r)) => { //Both children exist
                        let mut hash_input = l.calculate_hash(hasher);
                        hash_input.push_str(&r.calculate_hash(hasher));
                        hasher.input_str(&hash_input);
                        final_string = hasher.result_str();
                    },
                    (&mut None, &mut None) => {
                        hasher.input_str("");
                        final_string = hasher.result_str();
                    },
                    (&mut Some(ref mut child), &mut None) | (&mut None, &mut Some(ref mut child)) => { 
                        //Node has a single child
                        let mut hash_input = child.calculate_hash(hasher);
                        hash_input.push_str(&child.calculate_hash(hasher));
                        hasher.input_str(&hash_input);
                        final_string = hasher.result_str();
                    }
                }
            }
        }
        hasher.reset(); //Prepare hasher for next time
        self.contents=Some(final_string.clone()); //Save hash for future use
        return final_string;
    }
}
